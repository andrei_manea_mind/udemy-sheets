#!/usr/bin/env python
# coding: utf-8

# cmd: jupyter nbconvert --to python "Manual Neural Network.ipynb"

# # Manual Neural Network
# 
# In this notebook we will manually build out a neural network that mimics the TensorFlow API. This will greatly help your understanding when working with the real TensorFlow!

# ____
# ### Quick Note on Super() and OOP

# In[1]:
from IPython import get_ipython


class SimpleClass():
    
    def __init__(self,str_input):
        print("SIMPLE"+str_input)


# In[2]:


class ExtendedClass(SimpleClass):
    
    def __init__(self):
        print('EXTENDED')


# In[3]:


s = ExtendedClass()


# In[4]:


class ExtendedClass(SimpleClass):
    
    def __init__(self):
        
        super().__init__(" My String")
        print('EXTENDED')


# In[5]:


s = ExtendedClass()


# ________

# ## Operation

# In[6]:


class Operation():
    """
    An Operation is a node in a "Graph". TensorFlow will also use this concept of a Graph.
    
    This Operation class will be inherited by other classes that actually compute the specific
    operation, such as adding or matrix multiplication.
    """
    
    def __init__(self, input_elements = []):
        """
        Intialize an Operation
        """
        self.input_elements = input_elements # The list of input elements
        self.output_elements = [] # List of elements consuming this element's output
        
        # For every element in the input, we append this operation (self) to the list of
        # the consumers of the input elements
        for element in input_elements:
            element.output_elements.append(self)
        
        # There will be a global default graph (TensorFlow works this way)
        # We will then append this particular operation
        # Append this operation to the list of operations in the currently active default graph
        _default_graph.operations.append(self)
  
    def compute(self):
        """ 
        This is a placeholder function. It will be overwritten by the actual specific operation
        that inherits from this class.
        
        """
        
        pass


# ## Example Operations
# 
# ### Addition

# In[7]:


class add(Operation):
    
    def __init__(self, x, y):
         
        super().__init__([x, y])

    def compute(self, x_var, y_var):
         
        self.inputs = [x_var, y_var]
        return x_var + y_var


# ### Multiplication

# In[8]:


class multiply(Operation):
     
    def __init__(self, a, b):
        
        super().__init__([a, b])
    
    def compute(self, a_var, b_var):
         
        self.inputs = [a_var, b_var]
        return a_var * b_var


# ### Matrix Multiplication

# In[9]:


class matmul(Operation):
     
    def __init__(self, a, b):
        
        super().__init__([a, b])
    
    def compute(self, a_mat, b_mat):
         
        self.inputs = [a_mat, b_mat]
        return a_mat.dot(b_mat)


# ## Placeholders

# In[10]:


class Placeholder():
    """
    A placeholder is a node that needs to be provided a value for computing the output in the Graph.
    """
    
    def __init__(self):
        
        self.output_elements = []
        
        _default_graph.placeholders.append(self)


# ## Variables

# In[11]:


class Variable():
    """
    This variable is a changeable parameter of the Graph.
    """
    
    def __init__(self, initial_value = None):
        
        self.value = initial_value
        self.output_elements = []
        
         
        _default_graph.variables.append(self)


# ## Graph

# In[12]:


class Graph():
    
    
    def __init__(self):
        
        self.operations = []
        self.placeholders = []
        self.variables = []
        
    def set_as_default(self):
        """
        Sets this Graph instance as the Global Default Graph
        """
        global _default_graph
        _default_graph = self


# ## A Basic Graph
# 
# $$ z = Ax + b $$
# 
# With A=10 and b=1
# 
# $$ z = 10x + 1 $$
# 
# Just need a placeholder for x and then once x is filled in we can solve it!

# In[13]:


g = Graph()


# In[14]:


g.set_as_default()


# In[15]:


A = Variable(10)


# In[16]:


b = Variable(1)


# In[17]:


# Will be filled out later
x = Placeholder()


# In[18]:


y = multiply(A,x)


# In[19]:


z = add(y,b)


# ## Session

# In[20]:


import numpy as np


# ### Traversing Operation Nodes

# In[21]:


def traverse_postorder(operation):
    """ 
    PostOrder Traversal of Nodes. Basically makes sure computations are done in 
    the correct order (Ax first , then Ax + b). Feel free to copy and paste this code.
    It is not super important for understanding the basic fundamentals of deep learning.
    """
    
    nodes_postorder = []
    def recurse(node):
        if isinstance(node, Operation):
            for input_element in node.input_elements:
                recurse(input_element)
        nodes_postorder.append(node)

    recurse(operation)
    return nodes_postorder


# In[22]:


class Session:
    
    def run(self, operation, feed_dict = {}):
        """ 
          operation: The operation to compute
          feed_dict: Dictionary mapping placeholders to input values (the data)  
        """
        
        # Puts nodes in correct order
        nodes_postorder = traverse_postorder(operation)
        
        for node in nodes_postorder:

            if type(node) == Placeholder:
                
                node.output = feed_dict[node]
                
            elif type(node) == Variable:
                
                node.output = node.value
                
            else: # Operation
                
                node.inputs = [element.output for element in node.input_elements]

                 
                node.output = node.compute(*node.inputs)
                
            # Convert lists to numpy arrays
            if type(node.output) == list:
                node.output = np.array(node.output)
        
        # Return the requested node value
        return operation.output



# In[23]:


sess = Session()


# In[24]:


result = sess.run(operation=z,feed_dict={x:10})


# In[25]:


result


# In[26]:


10*10 + 1


# ** Looks like we did it! **

# In[27]:


g = Graph()

g.set_as_default()

A = Variable([[10,20],[30,40]])
b = Variable([1,1])

x = Placeholder()

y = matmul(A,x)

z = add(y,b)


# In[28]:


sess = Session()


# In[29]:


result = sess.run(operation=z,feed_dict={x:10})


# In[30]:


result


# ## Activation Function

# In[31]:


import matplotlib.pyplot as plt


# In[32]:


def sigmoid(z):
    return 1/(1+np.exp(-z))


# In[33]:


sample_z = np.linspace(-10,10,100)
sample_a = sigmoid(sample_z)


# In[34]:


plt.plot(sample_z,sample_a)


# #### Sigmoid as an Operation

# In[35]:


class Sigmoid(Operation):
 
    
    def __init__(self, z):

        # a is the input element
        super().__init__([z])

    def compute(self, z_val):
        
        return 1/(1+np.exp(-z_val))


# ## Classification Example

# In[36]:


from sklearn.datasets import make_blobs


# In[37]:


data = make_blobs(n_samples = 50,n_features=2,centers=2,random_state=75)


# In[38]:


data


# In[39]:


features = data[0]
plt.scatter(features[:,0],features[:,1])


# In[40]:


labels = data[1]
plt.scatter(features[:,0],features[:,1],c=labels,cmap='coolwarm')


# In[41]:


# DRAW A LINE THAT SEPERATES CLASSES
x = np.linspace(0,11,10)
y = -x + 5
plt.scatter(features[:,0],features[:,1],c=labels,cmap='coolwarm')
plt.plot(x,y)


# ## Defining the Perceptron
# 
# $$ y = mx + b $$
# 
# $$ y = -x + 5 $$
# 
# $$ f1 = mf2 + b , m=1$$
# 
# $$ f1 = -f2 + 5 $$
# 
# $$ f1 + f2 - 5 = 0 $$
# 

# ### Convert to a Matrix Representation of Features

# $$ w^Tx + b = 0 $$
# 
# $$   \Big(1, 1\Big)f - 5 = 0 $$
# 
# Then if the result is > 0 its label 1, if it is less than 0, it is label=0
# 
# 

# ### Example Point
# 
# Let's say we have the point f1=2 , f2=2 otherwise stated as (8,10). Then we have:
# 
# $$ 
# \begin{pmatrix} 
# 1 , 1
# \end{pmatrix} 
# \begin{pmatrix} 
# 8 \\
# 10
# \end{pmatrix} + 5 =  $$

# In[42]:


np.array([1, 1]).dot(np.array([[8],[10]])) - 5


# Or if we have (4,-10)

# In[43]:


np.array([1,1]).dot(np.array([[4],[-10]])) - 5


# ### Using an Example Session Graph

# In[44]:


g = Graph()


# In[45]:


g.set_as_default()


# In[46]:


x = Placeholder()


# In[47]:


w = Variable([1,1])


# In[48]:


b = Variable(-5)


# In[49]:


z = add(matmul(w,x),b)


# In[50]:


a = Sigmoid(z)


# In[51]:


sess = Session()


# In[52]:


sess.run(operation=a,feed_dict={x:[8,10]})


# In[53]:


sess.run(operation=a,feed_dict={x:[0,-10]})


# # Great Job!
