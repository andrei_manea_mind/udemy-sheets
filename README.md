# Tensorflow-Bootcamp

## Install Python 3

## Install Anaconda 
	with path variable enabled

## Install JetBrains Pycharm

## Create the environment "tfdeeplearning":
	conda env create -f tfdl_env.yml

	conda activate tfdeeplearning
	conda deactivate

## Install Jupyter Notebook

## Add project interpreter

## Install a few needed packages
	conda install -c anaconda numpy

	conda install -c conda-forge matplotlib
	conda install -c conda-forge/label/testing matplotlib
	conda install -c conda-forge/label/testing/gcc7 matplotlib
	conda install -c conda-forge/label/gcc7 matplotlib
	conda install -c conda-forge/label/broken matplotlib
	conda install -c conda-forge/label/rc matplotlib
	conda install -c conda-forge/label/cf201901 matplotlib

	conda install -c anaconda scikit-learn

	conda install -c anaconda pandas

	conda install -c conda-forge tensorflow
	conda install -c conda-forge/label/broken tensorflow
	conda install -c conda-forge/label/cf201901 tensorflow

####TF GPU
	conda install -c anaconda tensorflow-gpu
	install the proper toolkit
	check: sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

## Ideas:
	Unbluring
	Colorization of images
	Image Translation
	Interactive Image Generation
	Setting Crop protection as the theme
	Industrial components fault diagnosis
	Art Classifier

## Datasets:
	Kaggle (you will have access to wide range of datasets)
	Physionet (datasets for various diseases with good annotations)
	Magenta (https://magenta.tensorflow.org/datasets/)
	Cars (https://ai.stanford.edu/~jkrause/cars/car_dataset.html)

## Understanding:
	http://setosa.io/ev/image-kernels/
	http://scs.ryerson.ca/~aharley/vis/conv/flat.html

## Usefull:
	https://experiments.withgoogle.com/ai/sound-maker/view/
	https://favouriteblog.com/machine-learning-projects-for-beginners/
	https://medium.mybridge.co/30-amazing-machine-learning-projects-for-the-past-year-v-2018-b853b8621ac7
	http://www.cs.ox.ac.uk/teaching/studentprojects/undergraduate.html
	https://www.crazyengineers.com/threads/artificial-intelligence-project-ideastopics-for-engineering-students.58508/
	https://www.analyticsvidhya.com/blog/2018/05/24-ultimate-data-science-projects-to-boost-your-knowledge-and-skills/

## Yet another usefull articles:
	https://dev.to/javinpaul/good-programmers-vs-average-programmer-and-why-asking-questions-and-paying-attention-to-details-matters-j3h
	https://towardsdatascience.com/understanding-your-convolution-network-with-visualizations-a4883441533b
	https://www.youtube.com/watch?v=uGYJuOyIvzs
	https://dev.to/duomly/loops-in-python-comparison-and-performance-4f2m
	https://www.pyimagesearch.com/2019/10/21/keras-vs-tf-keras-whats-the-difference-in-tensorflow-2-0/?utm_source=facebook&utm_medium=ad-21-10-2019&utm_campaign=21+October+2019+BP+-+Traffic&utm_content=Default+name+-+Traffic+-+Image+3&fbid_campaign=6125643009046&fbid_adset=6125648754446&utm_adset=21+October+2019+BP+-+Email+List+-+Worldwide+-+18%2B&fbid_ad=6125648754646
	https://www.kdnuggets.com/2019/10/data-scientist-data-management.html
	https://www.pyimagesearch.com/2019/10/14/why-is-my-validation-loss-lower-than-my-training-loss/?utm_source=facebook&utm_medium=ad-14-10-2019&utm_campaign=14+October+2019+BP+-+Traffic&utm_content=Default+name+-+Traffic&fbid_campaign=6124758521246&fbid_adset=6124758523446&utm_adset=14+October+2019+BP+-+All+Visitors+90+Days+-+Worldwide+-+18%2B&fbid_ad=6124758525446
	https://realpython.com/inheritance-composition-python/
	https://colab.research.google.com/drive/1XBnjhMC_u3bxbW-P292rnnrtZJfJ4pSt